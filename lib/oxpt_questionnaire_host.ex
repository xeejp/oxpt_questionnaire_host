defmodule Oxpt.Questionnaire.Host do
  use Cizen.Automaton
  defstruct [:room_id, :questionnaire_id]

  use Cizen.Effects
  alias Cizen.{Dispatcher, Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Game
  alias Oxpt.Player.{Input, Output}
  alias Oxpt.Questionnaire.{Message, MessageTo}

  require Logger

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../..") |> Path.expand(),
    index: "src/index.jsx",
    oxpt_path: "../oxpt"

  @impl Oxpt.Game
  def player_socket(game_id, _game, guest_id) do
    %__MODULE__.HostSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, params) do
    questionnaire_id = Keyword.get(params, :questionnaire_id)
    %__MODULE__{room_id: room_id, questionnaire_id: questionnaire_id}
  end

  @impl true
  def spawn(id, %__MODULE__{questionnaire_id: game_id}) do
    Logger.debug("Questionnailre.Host Spawned")
    Logger.debug(game_id)
    Logger.debug(id)

    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %MessageTo{
        game_id: ^id, to_id: ^id
      }} -> true end)
    }

    {:loop, game_id}
  end

  @impl true
  def yield(id, {:loop, game_id}) do
    event = perform id, %Receive{}

    Logger.debug("questionnaire host event handled")
    IO.inspect(event)

    case event.body do
      %MessageTo{event: "fetch_state", from_id: from_id} ->
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: game_id,
            to_id: game_id,
            from_id: from_id,
            event: "fetch_state_host",
            payload: %{}
          }
        }

      %MessageTo{event: "update_state", from_id: from_id, payload: payload} ->
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: game_id,
            to_id: game_id,
            from_id: from_id,
            event: "update_state_host",
            payload: Map.pop(payload, "data") |> elem(0)
          }
        }

      _ ->
    end
    {:loop, game_id}
  end
end

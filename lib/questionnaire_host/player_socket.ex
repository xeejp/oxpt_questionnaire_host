#defmodule Oxpt.Questionnaire.Message do
#  @enforce_keys [:game_id, :guest_id, :event, :payload]
#  defstruct [:game_id, :guest_id, :event, :payload]
#end

defmodule Oxpt.Questionnaire.MessageTo do
  @enforce_keys [:game_id, :to_id, :from_id, :event, :payload]
  defstruct [:game_id, :to_id, :from_id, :event, :payload]
end

defmodule Oxpt.Questionnaire.Host.HostSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_id]

  use Cizen.Effects

  alias Cizen.{Dispatcher, Event, Filter}
  alias Oxpt.Player.{Input, Output}
  alias Oxpt.Questionnaire.MessageTo

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %Input{
        game_id: ^socket.game_id,
        guest_id: ^socket.guest_id
      }} -> true end)
    }

    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %MessageTo{
        game_id: ^socket.game_id,
        to_id: ^socket.guest_id
      }} -> true end)
    }

    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %MessageTo{
        game_id: ^socket.game_id,
        to_id: ^socket.game_id,
        event: "dispatch_state_host"
      }} -> true end)
    }

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform id, %Receive{
      event_filter: Filter.new(fn
        %Event{body: %Input{}} -> true
        # %Event{body: %Message{}} -> true
        %Event{body: %MessageTo{}} -> true
      end)
    }

    IO.inspect(id)
    IO.inspect(socket)
    IO.inspect(event.body)

    case event.body do
      %Input{event: "fetch_state", payload: payload} ->
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: socket.game_id,
            to_id: socket.game_id,
            from_id: socket.guest_id,
            event: "fetch_state",
            payload: %{}
          }
        }

      %Input{event: "update_state", payload: payload} ->
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: socket.game_id,
            to_id: socket.game_id,
            from_id: socket.guest_id,
            event: "update_state",
            payload: payload
          }
        }

      %MessageTo{event: "dispatch_state_host", payload: payload} ->
        perform id, %Dispatch{
          body: %Output{
            game_id: socket.game_id,
            guest_id: socket.guest_id,
            event: "update",
            payload: %{type: "dispatch_state", data: payload }
          }
        }
    end

    {:loop, socket}
  end
end

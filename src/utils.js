export function generateUUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.split('').map(c => {
    switch (c) {
      case 'x':
        return Math.floor(Math.random() * 16).toString(16)
      case 'y':
        return (Math.floor(Math.random() * 4) + 8).toString(16)
      default:
        return c
    }
  }).join('')
}

/**
 * recursiveAssign
 * Assign value in nested object
 * @param {Object} state
 * @param {Array[String] | String} keys
 * @param {Object} value
 * @return {Object} obj
 */
export function recursiveAssign(state, keys, value) {
  if (typeof keys === 'string') {
    if (keys.includes(':')) {
      const [key, idx] = keys.split(':')
      let arr = state[key]
      arr[Number(idx)] = value
      return Object.assign({}, state, { [key]: arr })
    }
    return Object.assign({}, state, { [keys]: value })
  } else if (Array.isArray(keys) && keys.length !== 0) {
    if (keys.length === 1)
      return recursiveAssign(state, keys[0], value)

    const deepKeys = keys.slice(1)
    if (keys[0].includes(':')) {
      const [key, idx] = keys[0].split(':')
      let arr = state[key]
      arr[Number(idx)] = recursiveAssign(state[key][Number(idx)], deepKeys, value)
      return Object.assign({}, state, { [key]: arr })
    }
    return Object.assign({}, state, { [keys[0]]: recursiveAssign(state[keys[0]], deepKeys, value) })
  }
  return state
}

import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { StylesProvider } from '@material-ui/core/styles'
import channel from 'oxpt'
import saga from './saga'
import reducer from './reducer'
import ErrorHandler from './ErrorHandler'
import App from './App'

const sagaMiddleware = createSagaMiddleware()
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose

const enhancer = composeEnhancers(
  applyMiddleware(sagaMiddleware)
)

const store = createStore(reducer, enhancer)
sagaMiddleware.run(saga)
channel.join()

channel.on('update', (payload) => {
  store.dispatch({ type: payload.type, data: payload.data })
})

window.sendData = function sendData(event, payload) {
  channel.push('input', { event: event, payload: payload })
}

class Root extends React.Component {
  render() {
    return (
      <ErrorHandler>
        <Provider store={store}>
          <StylesProvider injectFirst>
            <App />
          </StylesProvider>
        </Provider>
      </ErrorHandler>
    )
  }
}

ReactDOM.render(
  <Root />,
  document.getElementById('root')
)

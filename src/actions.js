// update local state
export const UPDATE_FIELD = 'update_field'

// edit questionnaire
export const ADD_FORM = 'add_form'
export const ADD_PAGE = 'add_page'
export const DELETE_FORM = 'delete_form'
export const DELETE_PAGE = 'delete_page'
// export const UPDATE_FORM_ORDER = 'update_form_order'
// export const UPDATE_PAGE_ORDER = 'update_page_order'

// update server state
export const FETCH_STATE = 'fetch_state'
export const UPDATE_STATE = 'update_state'
export const DISPATCH_STATE = 'dispatch_state'

export const UPDATE_FORM = 'update_questionnaire'

export const updateField = (key, value) => ({ type: UPDATE_FIELD, data: { key: key, value: value }})

export const addForm = () => ({ type: ADD_FORM })
export const addPage = () => ({ type: ADD_PAGE })
export const deleteForm = (key) => ({ type: DELETE_FORM, data: { key: key }})
export const deletePage = (key) => ({ type: DELETE_PAGE, data: { key: key }})

export const fetchState = () => ({ type: FETCH_STATE })
export const updateState = (state) => ({ type: UPDATE_STATE, data: { state: state }})

export const updateForm = () => ({ type: UPDATE_FORM })

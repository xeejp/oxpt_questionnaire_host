import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'

import SwipeableViews from 'react-swipeable-views'
import Paper from '@material-ui/core/Paper'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import EditForm from './components/EditForm'
import GameStepper from './components/GameStepper'
import Guests from './components/Guests'
import { fetchState } from './actions'

const Container = styled.div`
  width: 90%;
  margin: 0 auto;
`

const mapStateToProps = ({ }) => ({
})
const mapDispatchToProps = (dispatch) => ({
  FetchState: () => { dispatch(fetchState()) },
})

function TabContainer({ children }) {
  return <Container>
    {children}
  </Container>
}

class App extends Component {
  state = {
    value: 0,
  }

  componentDidMount = () => {
    this.props.FetchState()
  }

  handleChange = (e, newValue) => {
    this.setState({
      value: newValue
    })
  }

  handleChangeIndex = (index) => {
    this.setState({
      value: index
    })
  }

  render() {
    const { value } = this.state
    return <Fragment>
      <Paper square>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor='primary'
          textColor='primary'
          centered
        >
          <Tab label='Edit' />
          <Tab label='Result' />
        </Tabs>
      </Paper>
      <SwipeableViews
        axis={'x'}
        index={value}
        onChangeIndex={this.handleChangeIndex}
      >
        <TabContainer>
          <EditForm />
        </TabContainer>
        <TabContainer>
          <GameStepper />
          <Guests />
        </TabContainer>
      </SwipeableViews>
    </Fragment>
  }
}

App.propTypes = {
  FetchState: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

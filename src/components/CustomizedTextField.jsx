import React from 'react'
import PropTypes from 'prop-types'

import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'

import DeleteIcon from '@material-ui/icons/Delete'

const useStyles = makeStyles({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    height: '48px',
  },
  input: {
    marginLeft: 8,
    flex: 1,
    width: '60%',
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4,
  },
})

export default function CustomizedTextField({ placeholder, ariaLabel, value, defaultValue, onChange, type, onClickDelete }) {
  const classes = useStyles()
  return (
    <Paper className={classes.root}>
      <InputBase
        className={classes.input}
        placeholder={placeholder}
        inputProps={{ 'aria-label': ariaLabel }}
        value={value}
        defaultValue={defaultValue}
        onChange={onChange}
        type={type}
      />
      <IconButton
        className={classes.iconButton}
        color='secondary'
        aria-label='Delete'
        onClick={onClickDelete}
      >
        <DeleteIcon />
      </IconButton>
    </Paper>
  )
}

CustomizedTextField.propTypes = {
  placeholder: PropTypes.string,
  ariaLabel: PropTypes.string,
  value: PropTypes.any,
  defaultValue: PropTypes.any,
  onChange: PropTypes.func,
  type: PropTypes.string,
  onClickDelete: PropTypes.func,
}

import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
// import 'typeface-roboto'

import Button from '@material-ui/core/Button'
import Modal from '@material-ui/core/Modal'
import Paper from '@material-ui/core/Paper'
import Link from '@material-ui/core/Link'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Typography from '@material-ui/core/Typography'

const mapStateToProps = ({ guests }) => ({
  guests
})

const mapDispatchToProps = (dispatch) => ({
})

const Container = styled(Paper)`
  width: 80%;
  padding: 20px 4%;
  margin: 10px 0;
`

const StyledModal = styled(Modal)`
  width: 80%;
  margin: 10% auto;
`

const StyledTable = styled(Table)`
  border-collapse: collapse;
  width: 100%;
`

const Item = ({ guest, state, openModal }) => {
  return <TableRow>
    <TableCell><Link href={`/game/${_oxpt.game_id}/${guest}`}>{guest}</Link></TableCell>
    <TableCell>{state.finished ? '回答済み' : '回答中'}</TableCell>
    <TableCell>{state.page}</TableCell>
    <TableCell><Button onClick={openModal}>View</Button></TableCell>
  </TableRow>
}

Item.propTypes = {
  guest: PropTypes.string.isRequired,
  state: PropTypes.object.isRequired,
  openModal: PropTypes.func.isRequired,
}

const TableItem = styled(Item)`
`

const Guests = ({ guests }) => {
  const [ open, setOpen ] = useState(false)
  const [ key, setModalContent ] = useState('')
  const items = Object.keys(guests).map(id => <TableItem key={id} guest={id} state={guests[id]} openModal={() => { setOpen(true); setModalContent(id) }} />)

  return <Container>
    <Typography variant='h4'>Guests</Typography>
    <Typography variant='h5' color='textSecondary'>{items.length}</Typography>
    <Button onClick={() => true}>Download with CSV</Button>
    <StyledModal
      open={open}
      onClose={() => setOpen(false)}
    >
      <Paper>
        <StyledTable>
          <TableHead>
            <TableRow>
              {guests[key] && Object.keys(guests[key].answer).map(k => <TableCell>{k}</TableCell>)}
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {guests[key] && Object.keys(guests[key].answer).map(k => <TableCell>{guests[key].answer[k]}</TableCell>)}
            </TableRow>
          </TableBody>
        </StyledTable>
      </Paper>
    </StyledModal>
    <StyledTable>
      <TableHead>
        <TableRow>
          <TableCell>ID</TableCell>
          <TableCell>状態</TableCell>
          <TableCell>ページ</TableCell>
          <TableCell>詳細</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {items}
      </TableBody>
    </StyledTable>
  </Container>
}

Guests.propTypes = {
  guests: PropTypes.object.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(Guests)

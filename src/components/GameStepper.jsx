import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
// import 'typeface-roboto'
import styled from 'styled-components'

import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import { updateState } from '../actions'

const Container = styled.div`
  margin: 20px 0 0 0;
`

const mapStateToProps = ({ state }) => ({
  state
})

const mapDispatchToProps = (dispatch) => ({
  UpdateState: (state) => dispatch(updateState(state))
})

const getSteps = () => {
  return ['Waiting', 'Playing', 'Finish']
}

const getStepContent = (step) => {
  switch (step) {
    case 0:
      return '待機画面を表示します'
    case 1:
      return '回答画面を表示します'
    case 2:
      return '終了画面を表示します'
    default:
      return 'Unknown step'
  }
}

const GameStepper = ({ UpdateState }) => {
  const [activeStep, setActiveStep] = React.useState(0)
  const steps = getSteps()

  const isStepOptional = (step) => {
    return step === 1
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1)
    UpdateState(getSteps()[activeStep + 1])
  }

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1)
    UpdateState(getSteps()[activeStep - 1])
  }

  const handleReset = () => {
    setActiveStep(0)
  }

  return (
    <Container>
      <Stepper activeStep={activeStep}>
        {steps.map((label, index) => {
          const stepProps = {}
          const labelProps = {}
          if (isStepOptional(index)) {
            labelProps.optional = (
              <Typography variant='caption'>Optional</Typography>
            )
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          )
        })}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography>
              All steps completed - you&aposre finished
            </Typography>
            <Button onClick={handleReset}>
              Reset
            </Button>
          </div>
        ) : (
          <div>
            <Typography>
              {getStepContent(activeStep)}
            </Typography>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
              >
                Back
              </Button>
              <Button
                variant='contained'
                color='primary'
                onClick={handleNext}
              >
                {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
              </Button>
            </div>
          </div>
        )}
      </div>
    </Container>
  )
}

GameStepper.propTypes = {
  UpdateState: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(GameStepper)

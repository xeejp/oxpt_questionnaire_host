import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Fab from '@material-ui/core/Fab'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
// import Typography from '@material-ui/core/Typography'

import AddIcon from '@material-ui/icons/Add'
import DeleteIcon from '@material-ui/icons/Delete'

import EditQuery from './EditQuery'
import { generateUUID, recursiveAssign } from '../utils'

const Container = styled(Paper)`
  display: flex;
  margin-top: 4px;
  margin-bottom: 4px;
`

const MainContainer = styled.div`
  width: 80%;
  padding: 8px;
`

const SideContainer = styled.div`
  width: calc(20% - 16px);
  height: 100%;
  margin-right: 8px;
`

const IconButton = styled(Fab)`
  margin: 8px 4px;
  float: right;
`

const StyledTextField = styled(TextField)`
  margin-left: 8px;
  margin-right: 8px;
`

const TitleField = styled(StyledTextField)`
  width: 32%;
`

const DescriptionField = styled(StyledTextField)`
  width: 40%;
`

const mapStateToProps = ({ }) => ({
})
const mapDispatchToProps = (dispatch) => ({
})

// ToDo: React hooks
class EditPage extends Component {
  handleChangeQuery = (idx) => (name) => (value) => {
    const { page, handleChangePage } = this.props
    const newQueries = page.queries.map((q, i) => {
      if (i === idx) {
        return recursiveAssign(q, name.split('.'), value)
      }
      return q
    })
    handleChangePage('queries')(newQueries)
  }

  handleDeleteQuery = (idx) => () => {
    const { page, handleChangePage } = this.props
    handleChangePage('queries')(page.queries.filter((x, i) => i !== idx))
  }

  addQueryCallback = () => {
    const newQuery = {
      _key: generateUUID(),
      title: '',
      description: '',
      type: 'selective',
      attributes: {
        type: 'radio',
        max: 100,
        min: 0,
        level: 1,
        alternatives: [
          {
            label: '',
            type: 'text',
          }
        ],
        form: {
          label: '',
          placeholder: '',
        }
      }
    }
    const { page, handleChangePage } = this.props
    const newQueries = page.queries
    newQueries.push(newQuery)
    handleChangePage('queries')(newQueries)
  }

  render() {
    const { pageId, page, handleChangePage, handleDeletePage } = this.props
    const { title, description, queries } = page
    const queryList = queries.map((q, i) =>
      <EditQuery
        key={q._key}
        query={q}
        handleChange={this.handleChangeQuery(i)}
        handleDelete={this.handleDeleteQuery(i)}
      />
    )
    return <Container>
      <MainContainer>
        <form noValidate>
          <div>
            <TitleField
              label='ページのタイトル'
              placeholder='必須'
              value={title}
              onChange={(e) => handleChangePage('title')(e.target.value)}
              margin='normal'
              required
            />
            <DescriptionField
              label='ページの説明'
              placeholder='任意'
              value={description}
              onChange={(e) => handleChangePage('description')(e.target.value)}
              margin='normal'
            />
          </div>
        </form>
        {queryList}
      </MainContainer>
      <SideContainer>
        <IconButton
          aria-label='ページの削除'
          color='secondary'
          size='medium'
          onClick={handleDeletePage}
        >
          <DeleteIcon />
        </IconButton>
        <IconButton
          aria-label='質問の追加'
          color='primary'
          size='medium'
          onClick={this.addQueryCallback}
        >
          <AddIcon />
        </IconButton>
      </SideContainer>
    </Container>
  }
}

EditPage.propTypes = {
  page: PropTypes.any.isRequired,
  pageId: PropTypes.string.isRequired,
  handleChangePage: PropTypes.func.isRequired,
  handleDeletePage: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPage)

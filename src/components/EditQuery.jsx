import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Fab from '@material-ui/core/Fab'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import TextField from '@material-ui/core/TextField'

import DeleteIcon from '@material-ui/icons/Delete'

import CustomizedTextField from './CustomizedTextField'
// import { recursiveAssign } from '../utils'

const Container = styled(Paper)`
  display: flex;
  margin-top: 4px;
  margin-bottom: 4px;
`

const MainContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 90%;
  padding: 8px;
`

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
  width: 90%;
`

const SideContainer = styled.div`
  width: calc(10% - 16px);
  height: 100%;
  margin-right: 8px;
`

const IconButton = styled(Fab)`
  margin: 8px 4px;
  float: right;
`

const StyledTextField = styled(TextField)`
  margin-left: 8px;
  margin-right: 8px;
`

const TitleField = styled(StyledTextField)`
  width: 32%;
`

const DescriptionField = styled(StyledTextField)`
  width: 100%;
`

const SelectContainer = styled.div`
  margin-top: 16px;
`

const ChoiceTextField = styled(CustomizedTextField)`
  width: 88%;
`

const mapStateToProps = ({ }) => ({
})
const mapDispatchToProps = (dispatch) => ({
})

// ToDo: React hooks
class EditQuery extends Component {
  categories = [
    { category: 'selective', label: '選択式' },
    { category: 'descriptive', label: '記述式' },
  ]
  types = {
    selective: [
      { label: 'Radio', type: 'radio' },
      { label: 'CheckBox', type: 'check' },
      { label: 'SelectBox', type: 'select' },
      { label: 'Number', type: 'number' },
      { label: 'Scale', type: 'scale' }
    ],
    descriptive: [
      { label: 'TextBox', type: 'text' },
      { label: 'Multiline', type: 'multiline' },
      { label: 'Secret', type: 'secret' },
    ]
  }

  addAlternatives = () => {
    const { query, handleChange } = this.props
    const newAlternative = {
      label: '',
      type: 'text',
    }
    const newAlternatives = query.attributes.alternatives
    newAlternatives.push(newAlternative)
    handleChange('attributes.alternatives')(newAlternatives)
    handleChange('attributes.level')(newAlternatives.length)
  }

  deleteAlternatives = (idx) => () => {
    const { query, handleChange } = this.props
    const newAlternatives = query.attributes.alternatives.filter((x, i) => i !== idx)
    handleChange('attributes.alternatives')(newAlternatives)
    handleChange('attributes.level')(newAlternatives.length)
  }

  render() {
    const { query, handleChange, handleDelete } = this.props
    const { _key, title, description, type, attributes } = query
    return (
      <Container>
        <MainContainer>
          <Form noValidate>
            <TitleField
              label='質問のタイトル'
              placeholder='必須'
              value={title}
              onChange={e => handleChange('title')(e.target.value)}
              margin='normal'
              required
            />
            <SelectContainer>
              <InputLabel shrink htmlFor={_key + '_category'}>
                質問のタイプ
              </InputLabel>
              <Select
                input={<Input name='category' id={_key + '_category'} />}
                value={type}
                onChange={(e) => { handleChange('type')(e.target.value) }}
              >
                {this.categories.map(x => (
                  <MenuItem key={_key + x.category} value={x.category}>
                    {x.label}
                  </MenuItem>
                ))}
              </Select>
            </SelectContainer>
            <SelectContainer>
              <InputLabel shrink htmlFor={_key + '_type'}>
                質問の種類
              </InputLabel>
              <Select
                input={<Input name='text' id={_key + '_type'} />}
                value={attributes.type}
                onChange={(e) => { handleChange('attributes.type')(e.target.value) }}
              >
                {this.types[type].map(x => (
                  <MenuItem key={_key + x.type} value={x.type}>
                    {x.label}
                  </MenuItem>
                ))}
              </Select>
            </SelectContainer>
            <DescriptionField
              label='質問の説明'
              placeholder='任意'
              value={description}
              onChange={e => handleChange('description')(e.target.value)}
              margin='normal'
              multiline
            />
          </Form>
          {
            type === 'selective'
              ? attributes.type === 'number'
                ? <Fragment>
                  <TitleField
                    label='最大値'
                    placeholder='最大値'
                    value={attributes.max}
                    onChange={e => handleChange('attributes.max')(Number(e.target.value))}
                    margin='normal'
                    type='number'
                    required
                  />
                  <TitleField
                    label='最小値'
                    placeholder='最小値'
                    value={attributes.min}
                    onChange={e => handleChange('attributes.min')(Number(e.target.value))}
                    margin='normal'
                    type='number'
                    required
                  />
                </Fragment>
                : <div>
                  <Button
                    variant='contained'
                    color='primary'
                    onClick={this.addAlternatives}
                  >
                    選択肢の追加
                  </Button>
                  {
                    attributes.alternatives.map((x, i) =>
                      <ChoiceTextField
                        key={`${_key}_${i}`}
                        placeholder={`選択肢${i + 1}`}
                        ariaLabel='choice'
                        value={x.label}
                        onChange={e => handleChange(`attributes.alternatives:${i}.label`)(e.target.value)}
                        onClickDelete={this.deleteAlternatives(i)}
                      />
                    )
                  }
                </div>
              : <Fragment>
                <TitleField
                  label='ラベル'
                  placeholder='label'
                  value={attributes.form.label}
                  onChange={e => handleChange('attributes.form.label')(e.target.value)}
                  margin='normal'
                  required
                />
                <TitleField
                  label='プレースホルダー'
                  placeholder='Placeholder(optional)'
                  value={attributes.form.placeholder}
                  onChange={e => handleChange('attributes.form.placeholder')(e.target.value)}
                  margin='normal'
                />
              </Fragment>
          }
        </MainContainer>
        <SideContainer>
          <IconButton
            aria-label='質問の削除'
            size='medium'
            onClick={handleDelete}
          >
            <DeleteIcon />
          </IconButton>
        </SideContainer>
      </Container>
    )
  }
}

EditQuery.propTypes = {
  query: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditQuery)

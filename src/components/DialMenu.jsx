import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'

import SpeedDial from '@material-ui/lab/SpeedDial'
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon'
import SpeedDialAction from '@material-ui/lab/SpeedDialAction'

const mapStateToProps = (state) => ({
})
const mapDispatchToProps = (dispatch) => ({
})

const StyledDial = styled(SpeedDial)`
  position: absolute;
  right: 32px;
  float: right;
`

// ToDo: React hooks
class DialMenu extends Component {
  state = {
    open: false,
  }

  handleClick = () => {
    this.setState(state => ({
      open: !state.open
    }))
  };

  handleClose = () => {
    this.setState({ open: false })
  };

  handleOpen = () => {
    this.setState({ open: true })
  };

  render() {
    const { actions } = this.props
    const { open } = this.state

    return (
      <Fragment>
        <StyledDial
          ariaLabel='Edit Menu'
          icon={<SpeedDialIcon />}
          onBlur={this.handleClose}
          onClick={this.handleClick}
          onClose={this.handleClose}
          onFocus={this.handleOpen}
          onMouseEnter={this.handleOpen}
          onMouseLeave={this.handleClose}
          open={open}
          direction='left'
        >
          {actions.map(action => (
            <SpeedDialAction
              key={action.name}
              icon={action.icon}
              tooltipTitle={action.name}
              onClick={() => { action.callback && action.callback(); this.handleClick() }}
            />
          ))}
        </StyledDial>
      </Fragment>
    )
  }
}

DialMenu.propTypes = {
  actions: PropTypes.array.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(DialMenu)

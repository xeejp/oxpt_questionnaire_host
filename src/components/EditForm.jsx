/* eslint-disable camelcase */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

import AddPageIcon from '@material-ui/icons/PlaylistAdd'
import EditIcon from '@material-ui/icons/Edit'
import GetAppIcon from '@material-ui/icons/GetApp'

import DialMenu from './DialMenu'
import EditPage from './EditPage'
import { updateField, updateForm } from '../actions'
import { generateUUID, recursiveAssign } from '../utils'

const Container = styled.div`
  flex-wrap: 'wrap';
`

const StyledTextField = styled(TextField)`
  margin-left: 8px;
  margin-right: 8px;
`

const TitleField = styled(StyledTextField)`
  width: 30%;
`

const DescriptionField = styled(StyledTextField)`
  width: 40%;
`
const mapStateToProps = ({ questionnaire }) => ({
  questionnaire
})
const mapDispatchToProps = (dispatch) => ({
  UpdateField: (key, value) => { dispatch(updateField(key, value)) },
  UpdateForm: () => { dispatch(updateForm()) },
})

class EditForm extends Component {
  state = {
    editFlag: false,
    title: '',
    description: '',
    pages: {},
    page_order: [],
    errorMessages: [],
  }

  componentWillReceiveProps = (props) => {
    const { questionnaire } = props
    this.setState({
      editFlag: false,
      title: questionnaire.title || '',
      description: questionnaire.description || '',
      pages: questionnaire.pages || {},
      page_order: questionnaire.page_order || [],
    })
  }

  handleChange = (name) => (e) => {
    this.setState({
      editFlag: true,
      [name]: e.target.value,
    })
  }

  handleChangePage = (page) => (name) => (value) => {
    const { pages } = this.state
    this.setState({
      editFlag: true,
      pages: recursiveAssign(pages, [page, name], value),
    })
  }

  handleClickSave = () => {
    if (this.validateQuestionnaire() !== 0) return
    const { UpdateField, UpdateForm } = this.props
    const { title, description, pages, page_order } = this.state

    UpdateField(['questionnaire', 'title'], title)
    UpdateField(['questionnaire', 'description'], description)
    UpdateField(['questionnaire', 'pages'], pages)
    UpdateField(['questionnaire', 'page_order'], page_order)
    UpdateForm()
  }

  handleClickDiscardChanges = () => {
    const { questionnaire } = this.props
    this.setState({
      editFlag: false,
      title: questionnaire.title || '',
      description: questionnaire.description || '',
      pages: questionnaire.pages || {},
      page_order: questionnaire.page_order || [],
    })
    this.validateQuestionnaire()
  }

  addPageCallback = key => () => {
    const { pages, page_order } = this.state
    const newPage = {
      title: '',
      description: '',
      queries: [],
    }
    this.setState({
      editFlag: true,
      pages: Object.assign({}, pages, { [key]: newPage }),
      page_order: page_order.concat([key]),
    })
  }

  handleDeletePage = key => () => {
    const { pages, page_order } = this.state
    const newPages = Object.assign({}, pages)
    if (key in newPages) {
      delete newPages[key]
    }
    this.setState({
      editFlag: true,
      pages: newPages,
      page_order: page_order.filter(k => k !== key),
    })
  }

  validateQuestionnaire = () => {
    const { pages, page_order } = this.state

    let messages = []
    let validPages = page_order
      .filter(p => typeof p === 'string' && p.length !== 0) // filter not string and empty string
      .filter((elem, index, self) => self.indexOf(elem) === index) // unique
    if (validPages.length !== page_order.length) {
      messages.push(`page_orderが正しくありません．空のpage id，もしくは重複しているpage idがあります．`)
    }
    for (const p of page_order) {
      if (!(p in pages)) {
        messages.push(`page_orderが正しくありません．pagesに${p}が存在しません．`)
      }
    }
    this.setState({
      errorMessages: messages,
    })
    return messages.length
  }

  menuActions = [
    { icon: <GetAppIcon />, name: 'JSON形式でダウンロード', callback: () => console.log('downloading') },
    { icon: <EditIcon />, name: 'アンケートのJSONを編集', callback: () => console.log('editing') },
    { icon: <AddPageIcon />, name: 'ページの追加', callback: this.addPageCallback(generateUUID()) },
  ]

  render() {
    const { editFlag, title, description, pages, page_order, errorMessages } = this.state
    const errors = errorMessages.map(msg => <Typography variant='h4' color='secondary'>{msg}</Typography>)

    return <Container>
      {errors}
      <form noValidate>
        <Button
          variant='contained'
          color='primary'
          onClick={this.handleClickSave}
          disabled={!editFlag}
        >変更を保存</Button>
        <Button
          variant='contained'
          color='secondary'
          onClick={this.handleClickDiscardChanges}
          disabled={!editFlag}
        >もとに戻す</Button>
        <DialMenu actions={this.menuActions} />
        <div>
          <TitleField
            label='アンケートのタイトル'
            placeholder='必須'
            value={title}
            onChange={this.handleChange('title')}
            margin='normal'
            required
          />
          <DescriptionField
            label='アンケートの説明'
            placeholder='任意'
            value={description}
            onChange={this.handleChange('description')}
            margin='normal'
          />
        </div>
      </form>
      {page_order.map(k =>
        <EditPage
          key={k}
          pageId={k}
          page={pages[k]}
          handleChangePage={this.handleChangePage(k)}
          handleDeletePage={this.handleDeletePage(k)}
        />
      )}
    </Container>
  }
}

EditForm.propTypes = {
  questionnaire: PropTypes.any.isRequired,

  UpdateField: PropTypes.func.isRequired,
  UpdateForm: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditForm)

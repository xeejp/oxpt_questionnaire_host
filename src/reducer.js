import { UPDATE_FIELD, DISPATCH_STATE } from './actions'
import { recursiveAssign } from './utils'

const initialState = {
  isFetching: false,
  state: '',
  start_page: '',
  questionnaire: {},
  guests: {},
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_FIELD: {
      const { key, value } = action.data
      return recursiveAssign(state, key, value)
    }

    case DISPATCH_STATE:
      return Object.assign({}, state, { ...action.data, isFetching: false })

    default:
      return state
  }
}

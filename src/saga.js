import 'babel-polyfill'
import { take, fork, select } from 'redux-saga/effects'

import { FETCH_STATE, UPDATE_STATE, UPDATE_FORM } from './actions'

function * fetchState() {
  while (true) {
    yield take(FETCH_STATE)
    sendData(FETCH_STATE, {})
  }
}

function * updateState() {
  while (true) {
    const payload = yield take(UPDATE_STATE)
    sendData(UPDATE_STATE, payload)
  }
}

function * updateForm() {
  while (true) {
    yield take(UPDATE_FORM)
    const { questionnaire } = yield select()
    sendData(UPDATE_STATE, { data: { questionnaire: questionnaire }})
  }
}

export default function * saga() {
  yield fork(fetchState)
  yield fork(updateState)
  yield fork(updateForm)
}
